package utilis;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ChecklistDao {
	private static final String TAG = ChecklistDao.class.getSimpleName();

	public static final String TABLE_CHECKLIST = "Checklist";
	public static final int TODAY = -1;
	public static final int PENDING = 0;
	public static final int DELIVERED = 1;
	public static final int FAILED = 2;

	static final String CHECK_LIST_NAME = "check_list_name";
	static final String ASSIGN_TO = "assign_to";
	static final String CHECKLIST_ID = "checklist_id";
	static final String ASSIGN_LATER = "assign_later";
	static final String NOTIFY_USER_BEFORE = "notify_user_before";
	static final String NOTIFY_TIME_BEFORE = "notify_user_time_before";
	static final String FORCE_ANSWER = "force_answer";
	static final String LAST_STATUS = "last_status";
	static final String FIRST_DATE = "first_date";
	static final String FIRST_TIME = "first_time";



	static final String COLUMN_STATUS = "status";
	static final String CALLING_STATUS = "callingstatus";
	static final String COLUMN_MODIFIED = "modified";

	private DbHelper mHelper;

	public ChecklistDao(Context context) {
		mHelper = new DbHelper(context);
	}
	public static void createTable(SQLiteDatabase db) {
		/*String DATABASE_CREATE_CHECKLIST = "create table " + TABLE_CHECKLIST + "(" + CHECK_LIST_NAME + " text , "
				+ ASSIGN_TO + " text, " + COLUMN_FROM_TIME + " text, " + COLUMN_TO_TIME + " text);";
*/
		String DATABASE_CREATE_CHECKLIST = "create table " + TABLE_CHECKLIST + "("  + CHECKLIST_ID + " INTEGER PRIMARY KEY, "+ CHECK_LIST_NAME + " text , "
				+ ASSIGN_TO + " text , "+ ASSIGN_LATER + " Boolean , " + NOTIFY_USER_BEFORE + " Boolean , " + NOTIFY_TIME_BEFORE + " Boolean , " + FORCE_ANSWER + " Boolean , " +  LAST_STATUS + " Boolean , " + FIRST_DATE + " text , " +FIRST_TIME + " text);";
		db.execSQL(DATABASE_CREATE_CHECKLIST);
		Log.e("tablecreate","done");
	}

	public void insertChecklist(String checklistName, String assignTo,Boolean assignLater,Boolean notifyUserBefore,String userTimeBefore,Boolean forceAnswer,Boolean lastStatus,String firstDate,String firstTime) {
		SQLiteDatabase db = mHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(CHECK_LIST_NAME, checklistName);
		values.put(ASSIGN_TO, assignTo);
		values.put(ASSIGN_LATER, assignLater);
		values.put(NOTIFY_USER_BEFORE, notifyUserBefore);
		values.put(NOTIFY_TIME_BEFORE, userTimeBefore);
		values.put(FORCE_ANSWER, forceAnswer);
		values.put(LAST_STATUS, lastStatus);
		values.put(FIRST_DATE, firstDate);
		values.put(FIRST_TIME, firstTime);

		db.insert(TABLE_CHECKLIST, null, values);
		Log.e("successfully insert","insert");
		/*values.put(CALLING_STATUS, callingStatus);

		if (status != null)
			values.put(COLUMN_STATUS, status);
		
		String whereClause = COLUMN_ID + "=" + id;
		Cursor cursor = db.query(TABLE_DELIVERY, new String[] { COLUMN_ID }, whereClause, null, null, null, null);
		if (cursor.moveToFirst()) {
			long rowId = db.update(TABLE_DELIVERY, values, whereClause, null);
			Log.d(TAG, rowId + " updated");
		} else {
			long rowId = db.insert(TABLE_DELIVERY, null, values);
			Log.d(TAG, rowId + " inserted");
		}*/
	}
	public void  getChecklist()
	{
		SQLiteDatabase db = mHelper.getReadableDatabase();
		String whereClause = CHECK_LIST_NAME + "='" + "s" + "'";
		Cursor pCursor=db.rawQuery("select * from " + TABLE_CHECKLIST, null);
		//Cursor pCursor = db.query(TABLE_CHECKLIST, new String[] {CHECK_LIST_NAME,ASSIGN_TO}, whereClause, null, null, null, null);
		//ArrayList<Product> products = new ArrayList<Product>();
		while (pCursor.moveToNext()) {
			Log.e("findss", "finadd       " + pCursor.getString(pCursor.getColumnIndex(FIRST_TIME)));



		}


	}


}
