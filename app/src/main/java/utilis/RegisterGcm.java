
package utilis;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;


/**
 * Created by on 6/30/2015.
 */
public abstract class RegisterGcm extends AsyncTask<Void, Void, Void> {
	private Context mContext;

	public RegisterGcm(Context context) {
		mContext = context;
	}

	@Override
	protected Void doInBackground(Void... params) {
		if (getDeviceToken() == null) {
			try {
				GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(mContext);
				String regid = gcm.register(Constants.SENDER_ID);
				Log.d("GCM", "Device registered, registration ID=" + regid);

				// Persist the registration ID - no need to register again.
				storeRegistrationId(regid);
			} catch (IOException ex) {
				// If there is an error, don't just keep trying to register.
				// Require the user to click a button again, or perform
				// exponential back-off.
				return null;
			}
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		String token = getDeviceToken();
		onTokenReceive(token);
		if (token == null)
			Toast.makeText(mContext, "NOT CONNECTED",
					Toast.LENGTH_SHORT).show();
	}

	private void storeRegistrationId(String regId) {
		SharedPreferences prefs = getGCMPreferences();
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Constants.GCM_DEVICE_TOKEN, regId);
		editor.commit();
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGCMPreferences() {
		// This sample app persists the registration ID in shared preferences, but
		// how you store the registration ID in your app is up to you.
		return mContext.getSharedPreferences(Constants.GCM_PREF, Context.MODE_PRIVATE);
	}

	private String getDeviceToken() {
		SharedPreferences prefs = getGCMPreferences();
		return prefs.getString(Constants.GCM_DEVICE_TOKEN, null);
	}

	// callback when token receive
	protected abstract void onTokenReceive(String deviceToken);

}
