package utilis;

/**
 * Created by brst-pc79 on 2/25/16.
 */
public class Constants {

    public static String CHECKLIST_SERVER_URL = "http://beta.brstdev.com/checklist/index.php/";
    public static String CHECKLIST_INBOX_URL = "checklist/inbox";
    public static String CHECKLIST_LOGIN_URL = "api/login";
    public static String CHECKLIST_ARCHIVES_URL = "recurrence/index";
    public static String CHECKLIST_MANAGE_CHECKLIST_URL = "checklist/create";
    public static String CHECKLIST_PROGRESS_URL = "reports/checklist-progress";
    public static String CHECKLIST_MANAGE_USER_URL = "user/create";
    public static String CHECKLIST_USER_PROGRESS_URL = "reports/user-progress";
    public static String CHECKLIST_COMPARE_URL = "reports/compare-checklists";
    public static String CHECKLIST_USER_LIST = "user/index";
    public static String CHECKLIST_EDIT_DELETE = "checklist/index";
    public static String CHECKLIST_ASSIGN_USER= "checklist/assign";
    public static String CHECKLIST_TEMPLATE= "checklist/create_from_template";
    public static String SENDER_ID="645917207539";
    public static final String GCM_DEVICE_TOKEN = "device_token";
    public static final String GCM_PREF = "gcm_pref";

}
