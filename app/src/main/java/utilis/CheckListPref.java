package utilis;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class CheckListPref {


	public static final String FIRM_NAME = "firm_name";
	public static final String USER_NAME = "user_name";
	public static final String PASSWORD = "password";
	public static final String GET_STARTED = "getstarted";
	public static final String DEVICE_TOKEN = "deviceToken";

	private static CheckListPref mInstance;
	private SharedPreferences mPref, defaultPref;
	private static String newPref = "new_pref";


	private String firmName;
	private String userName;
	private String password;
	private String started;
	private String deviceToken;


	public void saveDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
		mPref.edit().putString(DEVICE_TOKEN,deviceToken).commit();
	}

	public String getDeviceToken() {
		return deviceToken;
	}


	public  String getStarted() {
		return started;
	}

	public void saveStarted(String getStarted) {
		this.started = getStarted;
		mPref.edit().putString(GET_STARTED,getStarted).commit();
	}

	public String getPassword() {
		return password;
	}

	public void savePassword(String password) {
		this.password = password;
		mPref.edit().putString(PASSWORD,password).commit();
	}

	public String getUserName() {
		return userName;
	}

	public void saveUserName(String userName) {
		this.userName = userName;
		mPref.edit().putString(USER_NAME, userName).commit();
	}

	private CheckListPref(Context context) {
		mPref = PreferenceManager.getDefaultSharedPreferences(context);
		reloadPrefrence();
	}

	public static SharedPreferences getMpref(Context context) {
		return context.getSharedPreferences(newPref, 0);
	}
	private void reloadPrefrence() {
		firmName = mPref.getString(FIRM_NAME, null);
		userName = mPref.getString(USER_NAME, null);
		password = mPref.getString(PASSWORD, null);
		started=mPref.getString(GET_STARTED,null);
	}
	public static CheckListPref getInstance(Context context) {
		return mInstance == null ? (mInstance = new CheckListPref(context)) : mInstance;
	}

	public String getFirmName() {
		return firmName;
	}

	public void saveFirmName(String firmName) {
		this.firmName = firmName;
		mPref.edit().putString(FIRM_NAME, firmName).commit();
	}

	public void clearAddresses() {
		firmName = null;
		mPref.edit().remove(FIRM_NAME).commit();
	}
	public void clearPref() {
		mPref.edit().clear().commit();
		reloadPrefrence();
	}



}
