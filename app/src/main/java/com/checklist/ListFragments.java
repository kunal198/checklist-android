package com.checklist;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import utilis.IOnFragmentInteraction;


public class ListFragments extends Fragment implements IOnFragmentInteraction {

	public ListFragments() {

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		//((TextView) getActivity().findViewById(R.id.tv_title)).setVisibility(View.VISIBLE);
	}

	@Override
	public void onDrawerOpenOrClose(boolean isOpen) {
		if (isOpen)
			((TextView) getActivity().findViewById(R.id.tv_title)).setVisibility(View.GONE);
		else
			((TextView) getActivity().findViewById(R.id.tv_title)).setVisibility(View.VISIBLE);
	}

}
