package com.checklist;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import utilis.CheckListPref;
import utilis.IResponse;
import utilis.RegisterGcm;
import utilis.ShowToast;
import server.PostServerResponse;

public class LoginScreen extends AppCompatActivity implements View.OnClickListener,IResponse{
    private Button btnSumbit;
    private IResponse postResponse;
    private EditText etxFirmName,etxUserName,etxUserPassword;
    private  String deviceid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        btnSumbit=(Button)findViewById(R.id.btnSubmit);
        etxFirmName=(EditText)findViewById(R.id.etxFirmName);
        etxUserName=(EditText)findViewById(R.id.etxEmail);
        etxUserPassword=(EditText)findViewById(R.id.etxPassword);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        postResponse=this;
        btnSumbit.setOnClickListener(this);

         deviceid = Settings.Secure.getString(
                LoginScreen.this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.e("device id", deviceid);

        if (CheckListPref.getInstance(this).getDeviceToken() == null) {
            new RegisterGcm(this) {
                protected void onTokenReceive(String deviceToken) {
                    Log.e("device tokenn", "data" + deviceToken);
                    CheckListPref.getInstance(LoginScreen.this).saveDeviceToken(deviceToken);
                }
            }.execute(); }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnSubmit:
                String loginUrl = "http://beta.brstdev.com/checklist/api/web/index.php/login";
                JSONObject jobj2 = new JSONObject();
                try {
                    jobj2.put("firm_name",etxFirmName.getText().toString());
                    jobj2.put("username", etxUserName.getText().toString());
                    jobj2.put("password", etxUserPassword.getText().toString());
                    jobj2.put("device_id", deviceid);
                    jobj2.put("token", CheckListPref.getInstance(LoginScreen.this).getDeviceToken());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("data", "" + jobj2);
               new PostServerResponse(LoginScreen.this, postResponse, jobj2, loginUrl).execute();
        }
    }
    @Override
    public void onReceiveResponse(String response) {
        try {
            JSONObject jObj=new JSONObject(response);
            Log.e("resultt",""+response);
            if(jObj.getString("success").equals("true"))
            {
                Log.e("responsee login", response);
                ShowToast.displayToast(this, "Successfully login");
                JSONObject jObjResponse=jObj.getJSONObject("response");
                CheckListPref.getInstance(LoginScreen.this).saveFirmName(jObjResponse.getString("firm_name"));
                CheckListPref.getInstance(LoginScreen.this).saveUserName(jObjResponse.getString("username"));
                CheckListPref.getInstance(LoginScreen.this).savePassword(jObjResponse.getString("password"));
                Intent i=new Intent(LoginScreen.this,Home.class);
                startActivity(i);
                finish();
                Log.e("firm name", CheckListPref.getInstance(LoginScreen.this).getFirmName());

                Log.e("user name", CheckListPref.getInstance(LoginScreen.this).getUserName());

            }
            else
                ShowToast.displayToast(this,"Please check username or password");
        } catch (JSONException e) {
            e.printStackTrace();
        }



    }
}
