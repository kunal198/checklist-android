package com.checklist;

import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;


@SuppressWarnings("deprecation")
public class Home extends ActionBarActivity implements LeftNav.NavigationDrawerCallbacks {
	private DrawerLayout mDrawerLayout;


	/**
	 * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
	 */
	private LeftNav mNavigationDrawerFragment;

	private ActionBarDrawerToggle actionBarDrawerToggle;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		mNavigationDrawerFragment = (LeftNav) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		//setSupportActionBar(toolbar);



		// getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.cream_color)));

		mNavigationDrawerFragment.setUp(R.id.navigation_drawer, mDrawerLayout);

		//new ActionBarDrawerToggle(this, mDrawerLayout,
		//R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close)



		actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
			}

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);

			}
		};

		mDrawerLayout.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

			@Override
			public void onGlobalLayout() {

				FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
				getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.black)));
				ft.replace(R.id.container, new ListFragments());
				ft.commit();

				if (Build.VERSION.SDK_INT < 16)
					mDrawerLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
				else
					mDrawerLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

				try {
					mDrawerLayout.closeDrawers();
				} catch (Exception e) {
					e.printStackTrace();
				}
				initiateFragment();
			}
		});
	}

	@Override
	public void onNavigationDrawerItemSelected(Fragment fragment) {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		ft.replace(R.id.container, fragment);
		ft.addToBackStack(null);
		ft.commit();
	}

	@Override
	public void onBackPressed() {

		if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
			mDrawerLayout.closeDrawers();

		super.onBackPressed();

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		actionBarDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		actionBarDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (actionBarDrawerToggle.onOptionsItemSelected(item)) {

			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * initiate fragment for web views
	 */
	private void initiateFragment() {

		Fragment frag = new WebViewFragments();
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.replace(R.id.container, frag);
		ft.commit();
	}

}
