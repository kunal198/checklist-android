package com.checklist;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import utilis.CheckListPref;
import utilis.RegisterGcm;


public class SplashScreen extends AppCompatActivity {

    private int SPLASH_TIME_OUT = 3000;
    private Intent mIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity
                    if (CheckListPref.getInstance(SplashScreen.this).getStarted() == null) {
                        mIntent = new Intent(SplashScreen.this, Tutorial.class);
                        startActivity(mIntent);
                    } else if (CheckListPref.getInstance(SplashScreen.this).getUserName() == null) {
                        mIntent = new Intent(SplashScreen.this, LoginScreen.class);
                        startActivity(mIntent);
                    } else {
                        mIntent = new Intent(SplashScreen.this, Home.class);
                        startActivity(mIntent);
                    }

                    finish();
                }
            }, SPLASH_TIME_OUT);
        }
    }

