package com.checklist;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ikovac.timepickerwithseconds.MyTimePickerDialog;
import com.ikovac.timepickerwithseconds.TimePicker;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EncodingUtils;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import utilis.CheckListPref;
import utilis.ChecklistDao;
import utilis.Constants;
import utilis.IResponse;
import utilis.JSONParser;
import utilis.ShowToast;


/**
 * Created by brst-pc79 on 2/25/16.
 */
public class WebViewFragments extends Fragment implements View.OnClickListener{
    private WebView mWebView;
    private String loadUrl, menuKey;
    private IResponse postResponse;
    private ProgressDialog progressDialog;
    private Spinner sprAssign,sprDaysHours,sprEvery,sprHoursDays;
    private ArrayList<String> AssigntoAdapter,notifyAdapter;
    private ArrayList<String> assignList,notifyList,repeatList,expireList;
    private Bundle bundle;
    private int year,month,day;
    private Calendar calendar;
    private DatePickerDialog datePicker;
    private EditText etxCheckListName,etxNotifyTime,etxEvery,etxExpireAfter;
    private ImageView imgClose,imgAssignTo,imgNotifyUser,imgExpireAfter,imgOnceEvery;
    private Dialog dialog;
    private Button btnClose,btnCreate;
    private CheckBox chkAssignLater,chkNotifyUser,chkForceAnswer,chkLastStatus,chkChecklistShifted;
    private RadioButton rdoOnce,rdoEvery,rdoKeepOn,rdoShiftTo,rdoDoNot;
    private TextView tvDate,tvTime,tvFromTime,tvToTime;
    private LinearLayout layEveryHour;
    private ArrayAdapter<String> dataAdapter;
    private ChecklistDao checklistDao;
    private String notifyBeforeTime,notifySpinnerText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.web_view_load, container, false);
        tvDate=(TextView)view.findViewById(R.id.tvDate);
        tvTime=(TextView)view.findViewById(R.id.tvTime);
        etxCheckListName=(EditText)view.findViewById(R.id.etxCheckListName);
        etxNotifyTime =(EditText)view.findViewById(R.id.etxNotifyTime);
        etxEvery =(EditText)view.findViewById(R.id.etxEvery);
        etxExpireAfter =(EditText)view.findViewById(R.id.etxExpireAfter);

        tvDate =(TextView)view.findViewById(R.id.tvDate);
        tvTime =(TextView)view.findViewById(R.id.tvTime);
        tvFromTime =(TextView)view.findViewById(R.id.tvFromTime);
        tvToTime =(TextView)view.findViewById(R.id.tvToTime);


        chkAssignLater=(CheckBox)view.findViewById(R.id.chkAssignLater);
        chkNotifyUser=(CheckBox)view.findViewById(R.id.chkNotifyUserBefore);
        chkForceAnswer=(CheckBox)view.findViewById(R.id.chkForceAnswer);
        chkLastStatus=(CheckBox)view.findViewById(R.id.chkLastStatus);
        chkChecklistShifted=(CheckBox)view.findViewById(R.id.chkChecklistShifted);

        rdoOnce=(RadioButton)view.findViewById(R.id.rdoOnce);
        rdoEvery=(RadioButton)view.findViewById(R.id.rdoEvery);


        rdoKeepOn=(RadioButton)view.findViewById(R.id.rdoKeepOn);
        rdoShiftTo=(RadioButton)view.findViewById(R.id.rdoShiftTo);
        rdoDoNot=(RadioButton)view.findViewById(R.id.rdoDoNot);

        imgAssignTo=(ImageView)view.findViewById(R.id.imgAssignTo);
        imgNotifyUser=(ImageView)view.findViewById(R.id.imgNotifyUser);
        imgExpireAfter=(ImageView)view.findViewById(R.id.imgExpireAfter);
        imgOnceEvery=(ImageView)view.findViewById(R.id.imgOnceEvery);
        btnCreate=(Button)view.findViewById(R.id.btnCreate);
        layEveryHour=(LinearLayout)view.findViewById(R.id.layEveryHours);

        tvDate.setOnClickListener(this);
        tvTime.setOnClickListener(this);
        tvFromTime.setOnClickListener(this);
        tvToTime.setOnClickListener(this);
        imgAssignTo.setOnClickListener(this);
        imgNotifyUser.setOnClickListener(this);
        imgOnceEvery.setOnClickListener(this);
        imgExpireAfter.setOnClickListener(this);
        btnCreate.setOnClickListener(this);

        chkAssignLater.setOnCheckedChangeListener(new myCheckBoxChnageClicker());
        chkNotifyUser.setOnCheckedChangeListener(new myCheckBoxChnageClicker());

        rdoOnce.setOnCheckedChangeListener(new myCheckBoxChnageClicker());
        rdoEvery.setOnCheckedChangeListener(new myCheckBoxChnageClicker());


        sprAssign=(Spinner)view.findViewById(R.id.sprAssign);
        sprDaysHours=(Spinner)view.findViewById(R.id.sprDaysHours);
        sprEvery=(Spinner)view.findViewById(R.id.sprEvery);
        sprHoursDays=(Spinner)view.findViewById(R.id.sprHoursDays);

        sprAssign.setOnItemSelectedListener(itemSelectedListener);
        sprDaysHours.setOnItemSelectedListener(itemSelectedListener);
        sprEvery.setOnItemSelectedListener(itemSelectedListener);
        sprHoursDays.setOnItemSelectedListener(itemSelectedListener);

        checklistDao = new ChecklistDao(getActivity());
        checklistDao.getChecklist();

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        mWebView = (WebView) view.findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mWebView.setScrollbarFadingEnabled(false);
        //mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.getSettings().setSupportMultipleWindows(true);

        assignList = new ArrayList<String>();
        assignList.add("Select User from list");
        assignList.add("Add New Users");
        dataAdapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item,assignList);
        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        sprAssign.setAdapter(dataAdapter);
        notifyList = new ArrayList<String>();
        notifyList.add("Minutes");
        notifyList.add("Hours");
        notifyList.add("Days");
        dataAdapter= new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item,notifyList);
        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        sprDaysHours.setAdapter(dataAdapter);
        sprDaysHours.setEnabled(false);
        imgNotifyUser.setEnabled(false);

        repeatList = new ArrayList<String>();
        repeatList.add("Months");
        repeatList.add("Days");
        repeatList.add("Hours");
        dataAdapter=new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item,repeatList);
        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        sprEvery.setAdapter(dataAdapter);
        sprEvery.setEnabled(false);
        imgOnceEvery.setEnabled(false);

        expireList = new ArrayList<String>();
        expireList.add("Days");
        expireList.add("Hours");
        dataAdapter=new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item,expireList);
        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        sprHoursDays.setAdapter(dataAdapter);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        bundle = getArguments();
        if (bundle != null) {
            loadUrl = bundle.getString(LeftNav.URL_KEY);
            Log.e("bundle not null","bundle"+loadUrl);
        } else {
             loadUrl=Constants.CHECKLIST_SERVER_URL+Constants.CHECKLIST_LOGIN_URL;
            Log.e("bundle  null","bundle");
        }
        ((TextView) getActivity().findViewById(R.id.tv_title)).setText(menuKey);
        Login_Url login=new Login_Url();
        login.execute();
    }



    public class Login_Url extends AsyncTask<String, String, String> {

        String url = "http://beta.brstdev.com/checklist/api/web/index.php/login";
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            Log.e("pree execute", "pree");
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try
            {
                List<NameValuePair> obj = new ArrayList<NameValuePair>();
                obj.add(new BasicNameValuePair("password",CheckListPref.getInstance(getActivity()).getPassword()));
                obj.add(new BasicNameValuePair("firm_name",CheckListPref.getInstance(getActivity()).getFirmName()));
                obj.add(new BasicNameValuePair("username",CheckListPref.getInstance(getActivity()).getUserName()));
                obj.add(new BasicNameValuePair("ismobile","true"));
                JSONParser obj1 = new JSONParser();
                JSONObject obj2 = obj1.makeHttpRequest(url, "POST", obj);
                Log.e("username","dd"+CheckListPref.getInstance(getActivity()).getUserName());
                Log.e("firmname","dd"+CheckListPref.getInstance(getActivity()).getFirmName());
                Log.e("username","dd"+CheckListPref.getInstance(getActivity()).getPassword());
                Log.e("responsee111",""+obj2);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if(bundle==null) {
                mWebView.loadUrl(loadUrl);
                mWebView.setWebViewClient(new WebViewClient() {
                    public boolean shouldOverrideUrlLoading(WebView viewx, String urlx) {

                        viewx.loadUrl(urlx);
                        return false;
                    }
                });
                Log.e("url posttss",loadUrl);
            }

            else {
              // mWebView.loadUrl(loadUrl);
                mWebView.loadUrl(loadUrl);
                mWebView.setWebViewClient(new WebViewClient() {
                    public boolean shouldOverrideUrlLoading(WebView viewx, String urlx) {
                        viewx.loadUrl(urlx);
                        return false;
                    }
                });
                Log.e("url post11",loadUrl);
               /* if(loadUrl.contains("create_from_template"))
                {
                    Toast.makeText(getActivity(),"template",Toast.LENGTH_SHORT).show();

                    mWebView.setVisibility(View.GONE);
                }
                else
                    mWebView.setVisibility(View.VISIBLE);*/
            }
        }}

void showCreateUserDialog()
{
     dialog = new Dialog(getActivity());
    dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    //dialog.setCancelable(false);
    dialog.setContentView(R.layout.dialog_create_user);
    imgClose=(ImageView)dialog.findViewById(R.id.imgClose);
    imgClose.setOnClickListener(this);
    btnClose=(Button)dialog.findViewById(R.id.btnClose);
    btnClose.setOnClickListener(this);

    if (Build.VERSION.SDK_INT < 14) {
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.85f;
        dialog.getWindow().setAttributes(lp);
    } else {
        dialog.getWindow().setDimAmount(0.85f);
    }
    dialog.show();


    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
    dialog.getWindow().setLayout((getResources().getDisplayMetrics().widthPixels * 80) / 100, WindowManager.LayoutParams.WRAP_CONTENT);
}

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvDate:
                datePicker = new DatePickerDialog(getActivity(), dateSetListener, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH) + 2);
                datePicker.show();
                break;
            case R.id.tvTime:
                showTime(true,false,false);
                break;
            case R.id.imgClose:
                dialog.dismiss();
                break;
            case R.id.btnClose:
                dialog.dismiss();
                break;
            case R.id.imgAssignTo:
                sprAssign.performClick();
                break;
            case R.id.imgNotifyUser:
                sprDaysHours.performClick();
                break;
            case R.id.imgOnceEvery:
                sprEvery.performClick();
                break;
            case R.id.imgExpireAfter:

                sprHoursDays.performClick();
                break;
            case R.id.btnCreate:
               if(checkValidation())
               {
                   if(!chkNotifyUser.isChecked())
                       notifyBeforeTime="";
                   else
                       notifyBeforeTime=etxNotifyTime.getText().toString()+" "+notifySpinnerText;
                   Log.e("notify",notifyBeforeTime);
                   checklistDao.insertChecklist(etxCheckListName.getText().toString(), "sd", chkAssignLater.isChecked(), chkNotifyUser.isChecked(), notifyBeforeTime,chkForceAnswer.isChecked(),chkLastStatus.isChecked(),tvDate.getText().toString(),tvTime.getText().toString());
              }
                break;
            case R.id.tvFromTime:
               showTime(false,true,false);
                break;
            case R.id.tvToTime:
                showTime(false,false,true);
                break;
        }
    }
    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {

                    showDate(selectedDay, selectedMonth, selectedYear);

        }
    };
    private void showDate(int day, int month, int year) {
        StringBuilder builder = new StringBuilder().append(day).append("/").append(month + 1).append("/").append(year);
            tvDate.setText(builder.toString());
    }
    class myCheckBoxChnageClicker implements CheckBox.OnCheckedChangeListener
    {
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            // TODO Auto-generated method stub
                if(buttonView==chkAssignLater)
                    if (isChecked) {
                        Toast.makeText(getActivity(), "checked", Toast.LENGTH_SHORT).show();
                        sprAssign.setBackgroundResource(R.drawable.shape_disabled_spinner);
                        sprAssign.setEnabled(false);
                        imgAssignTo.setEnabled(false);
                    }
                else {
                        sprAssign.setBackgroundResource(R.drawable.shape_white_corner);
                        sprAssign.setEnabled(true);
                        imgAssignTo.setEnabled(true);
                        Toast.makeText(getActivity(), "unchecked", Toast.LENGTH_SHORT).show();
                    }
                if(buttonView==chkNotifyUser)
                    if(isChecked) {
                        Toast.makeText(getActivity(), "checked1", Toast.LENGTH_SHORT).show();
                        sprDaysHours.setBackgroundResource(R.drawable.shape_white_corner);
                        sprDaysHours.setEnabled(true);
                        etxNotifyTime.setEnabled(true);
                        etxNotifyTime.setBackgroundResource(R.drawable.shape_white_corner);
                        imgNotifyUser.setEnabled(true);
                    }
                else {
                        Toast.makeText(getActivity(), "unchecked1", Toast.LENGTH_SHORT).show();
                        sprDaysHours.setBackgroundResource(R.drawable.shape_disabled_spinner);
                        sprDaysHours.setEnabled(false);
                        etxNotifyTime.setEnabled(false);
                        etxNotifyTime.setBackgroundResource(R.drawable.shape_disabled_spinner);
                        imgNotifyUser.setEnabled(false);
                    }
            if(buttonView==rdoEvery)
                if(isChecked) {
                    Toast.makeText(getActivity(), "rchecked", Toast.LENGTH_SHORT).show();
                    sprEvery.setBackgroundResource(R.drawable.shape_white_corner);
                    etxEvery.setBackgroundResource(R.drawable.shape_white_corner);
                    sprEvery.setEnabled(true);
                    etxEvery.setEnabled(true);
                    imgOnceEvery.setEnabled(true);
                }
                else {
                    Toast.makeText(getActivity(), "runchecked", Toast.LENGTH_SHORT).show();
                    sprEvery.setBackgroundResource(R.drawable.shape_disabled_spinner);
                    etxEvery.setBackgroundResource(R.drawable.shape_disabled_spinner);
                    etxEvery.setEnabled(false);
                    sprEvery.setEnabled(false);
                    imgOnceEvery.setEnabled(false);
                }
            if(buttonView==rdoOnce)
                if(isChecked)
                    Toast.makeText(getActivity(),"once checked",Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getActivity(),"once unchecked",Toast.LENGTH_SHORT).show();
        }
    }
    private AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                                   long arg3) {
              if(arg0==sprAssign)
              {
                  Log.e("detailsss","detailss "+arg2);
              }
            if(arg0==sprDaysHours)
            {
                Log.e("detailsss2","detailss "+arg2);
                notifySpinnerText=notifyList.get(arg2).toString();
                ShowToast.displayToast(getActivity(), notifySpinnerText);
            }
            if(arg0==sprEvery)
            {
                if(arg2==0)
                {
                    expireList = new ArrayList<String>();
                    expireList.add("Days");
                    expireList.add("Hours");
                    setAdapter(expireList);
                }
                if(arg2==1)
                {
                    expireList = new ArrayList<String>();
                    expireList.add("Hours");
                    setAdapter(expireList);

                }
                if(arg2==2) {
                    layEveryHour.setVisibility(View.VISIBLE);
                    expireList = new ArrayList<String>();
                    expireList.add("Hours");
                    setAdapter(expireList);
                }
                else
                    layEveryHour.setVisibility(View.GONE);
            }
            if(arg0==sprHoursDays)
            {
                Log.e("detailsss4","detailss "+arg2);

            }

        }
        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub

        }
    };
    void setAdapter(ArrayList<String> list)
    {
        dataAdapter=new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item,list);
        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        sprHoursDays.setAdapter(dataAdapter);

    }
    void showTime(final Boolean firstTime,final Boolean fromTime,final Boolean toTime)
    {
        Calendar now = Calendar.getInstance();
        MyTimePickerDialog mTimePicker = new MyTimePickerDialog(getActivity(), new MyTimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute, int seconds) {
                // TODO Auto-generated method stub
                if(firstTime) {
                    tvTime.setText(String.format("%02d", hourOfDay) +
                            ":" + String.format("%02d", minute) +
                            ":" + String.format("%02d", seconds));
                }
                else if(fromTime)
                {
                    tvFromTime.setText(String.format("%02d", hourOfDay) +
                            ":" + String.format("%02d", minute) +
                            ":" + String.format("%02d", seconds));
                }
                else if(toTime)
                {
                    tvToTime.setText(String.format("%02d", hourOfDay) +
                            ":" + String.format("%02d", minute) +
                            ":" + String.format("%02d", seconds));
                }
            }
        }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), now.get(Calendar.SECOND), true);
        mTimePicker.show();
    }
    boolean checkValidation()
    {
        if(etxCheckListName.getText().toString().equals("")) {
            ShowToast.displayToast(getActivity(), getResources().getString(R.string.create_checklist_text));
            return false;
        }
        else if(chkNotifyUser.isChecked()&&etxNotifyTime.getText().toString().equals("")) {
            ShowToast.displayToast(getActivity(), getResources().getString(R.string.notify_user));
            return false;
        }
        else if(tvDate.getText().toString().equals("")) {
            ShowToast.displayToast(getActivity(), getResources().getString(R.string.date_text));
            return false;
        }
        else if(tvTime.getText().toString().equals("")) {
            ShowToast.displayToast(getActivity(), getResources().getString(R.string.time_text));
            return false;
        }
        return true;
    }
}
