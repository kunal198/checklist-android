package com.checklist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import utilis.CheckListPref;
import utilis.CirclePageIndicator;
import adapter.TutorialAdapter;
/**
 * Created by brst-pc79 on 2/24/16.
 */
public class Tutorial extends FragmentActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    private ViewPager mPager;
    private Button btnGetStart;
    private CirclePageIndicator mIndicator;
    private TutorialAdapter mAdapter;
    private int count = 3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial);

        mPager = (ViewPager) findViewById(R.id.pager);
        btnGetStart = (Button) findViewById(R.id.btn_start);
        mIndicator = (CirclePageIndicator) findViewById(R.id.circle_indicator);

        setViewpagerAdapter();

        btnGetStart.setOnClickListener(this);


    }

    /**
     * set view pager startmove.brst.com.smartmove.adapter of tutorial
     */
    @SuppressWarnings("deprecation")
	private void setViewpagerAdapter() {
        mAdapter = new TutorialAdapter(getSupportFragmentManager(), this, count);
        mPager.setAdapter(mAdapter);
        mIndicator.setViewPager(mPager);
        float density = getResources().getDisplayMetrics().density;
        mIndicator.setRadius(3 * density);
        mIndicator.setPageColor(0x880000FF);
        mIndicator.setFillColor(getResources().getColor(R.color.white));
        mIndicator.setStrokeColor(getResources().getColor(R.color.black));
        mIndicator.setStrokeWidth(7 * density);
        mIndicator.setSnap(true);

        mIndicator.setOnPageChangeListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_start:
            	Intent intent = new Intent(this,LoginScreen.class);
            	startActivity(intent);
            	finish();
                CheckListPref.getInstance(Tutorial.this).saveStarted("true");
                break;
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        count = position;


    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
